import { async, TestBed } from '@angular/core/testing';
import { LazyloadedModule } from './lazyloaded.module';

describe('LazyloadedModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [LazyloadedModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(LazyloadedModule).toBeDefined();
  });
});
