module.exports = {
  name: 'lazyloaded',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/lazyloaded',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
