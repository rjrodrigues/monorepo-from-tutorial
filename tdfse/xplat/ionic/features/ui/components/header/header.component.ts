import { Component } from '@angular/core';

import { HeaderBaseComponent } from '@tdfse/features';

@Component({
  selector: 'tdfse-ion-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent extends HeaderBaseComponent {}
