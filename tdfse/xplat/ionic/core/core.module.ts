import { NgModule, Optional, SkipSelf } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { throwIfAlreadyLoaded } from '@tdfse/utils';
import { TdfseCoreModule } from '@tdfse/web';

@NgModule({
  imports: [TdfseCoreModule, IonicModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ]
})
export class TdfseIonicCoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: TdfseIonicCoreModule
  ) {
    throwIfAlreadyLoaded(parentModule, 'TdfseIonicCoreModule');
  }
}
