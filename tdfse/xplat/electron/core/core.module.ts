import { NgModule, Optional, SkipSelf } from '@angular/core';

import { throwIfAlreadyLoaded } from '@tdfse/utils';
import { ElectronService } from './services';

@NgModule({
  providers: [ElectronService]
})
export class TdfseElectronCoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: TdfseElectronCoreModule,
    private _electronService: ElectronService
  ) {
    throwIfAlreadyLoaded(parentModule, 'TdfseElectronCoreModule');
  }
}
