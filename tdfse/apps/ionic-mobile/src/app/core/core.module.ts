import { NgModule } from '@angular/core';

// libs
import { TdfseIonicCoreModule } from '@tdfse/ionic';

@NgModule({
  imports: [TdfseIonicCoreModule]
})
export class CoreModule {}
