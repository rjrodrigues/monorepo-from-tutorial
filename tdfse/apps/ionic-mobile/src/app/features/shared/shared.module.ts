import { NgModule } from '@angular/core';

// xplat
import { UIModule } from '@tdfse/ionic';

@NgModule({
  imports: [UIModule],
  exports: [UIModule]
})
export class SharedModule {}
