import { Component } from '@angular/core';
import { BaseComponent } from '@tdfse/core';

@Component({
  selector: 'page-home',
  templateUrl: 'home.component.html'
})
export class HomeComponent extends BaseComponent {}
