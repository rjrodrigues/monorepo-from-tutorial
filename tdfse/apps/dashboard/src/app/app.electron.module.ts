import { NgModule } from '@angular/core';
import { TdfseElectronCoreModule } from '@tdfse/electron';
import { AppModule } from './app.module';
import { AppComponent } from './app.component';

@NgModule({
  imports: [AppModule, TdfseElectronCoreModule],
  bootstrap: [AppComponent]
})
export class AppElectronModule {}
